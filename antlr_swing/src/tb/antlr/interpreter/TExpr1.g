tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
import tb.antlr.symbolTable.LocalSymbols;
}

//obiekt klasy do obslugi zmiennych
@members {
LocalSymbols locVariables = new LocalSymbols();
}


prog    : ( 
          ^(PRINT e=expr) {drukuj($e.out.toString());}
          | ^(VAR p=ID) {locVariables.newSymbol($p.text);} //utworzenie zmiennej
          | ^(PODST p=ID e1=expr) {locVariables.setSymbol($p.text, $e1.out);} //podstawienie do zmiennej
          | LB {locVariables.enterScope();} //obsluga blokow instrukcji
          | RB {locVariables.leaveScope();}
          )*
          ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        //dodatkowe dzialania
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = (int)Math.pow($e1.out, $e2.out);}
        | ^(ABS   e1=expr) {$out = Math.abs($e1.out);}     
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = locVariables.getSymbol($ID.text);}
        ;
        