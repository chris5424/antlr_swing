grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    //odczyt blokow instrukcji
    | LB NL -> LB
    | RB NL -> RB
    | if_stat NL -> if_stat
    | NL ->
    ;
    
if_stat:
    IF^ expr THEN!(stat) (ELSE! (stat));

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

//wyrazenie potegujace ma pierwszenstwo przed mnozeniem/dodawaniem
powerexpr
    : atom
    ( POW^ atom )*
    ;

multExpr
    : powerexpr
      ( MUL^ powerexpr
      | DIV^ powerexpr
      | MOD^ powerexpr
      )*
      ;

PRINT
  : 'print'
  ;

atom
    : INT
    | ID
    | LP! expr RP!
    | ABS expr ABS -> ^(ABS expr)
    ;
    
VAR :'var';

IF
  : 'if'
  ;

ELSE
  : 'else'
  ;
  
THEN
  : 'then'
  ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
//dodatkowe dzialania
LB
  : '{'
  ;
RB
  : '}'
  ;
  
MOD
  : '%'
  ;
  
ABS
  : '|'
  ;
  
POW
  : '^'
  ;